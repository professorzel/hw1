#pragma once
#include <string>
#include <fstream>
#include <iostream>
using namespace std;
const int N = 10;

struct User
{
	int id;
	string name;
	string number;
	string plan;
	string address;
};

bool read_file(string file_name, User* mngr);
bool write_file(string file_name, User* mngr);
void printf(User* mngr);
void about();

