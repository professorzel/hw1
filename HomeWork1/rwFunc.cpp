#include "rwFunc.h"
bool read_file(string file_name, User* list)
{
	ifstream file(file_name);
	if (!file) {
		return false;
	}
	for (int i = 0; i < N; i++) {
		file >> list[i].id;
		file.ignore(1);
		getline(file, list[i].name, ';');
		getline(file, list[i].number, ';');
		getline(file, list[i].plan, ';');
		getline(file, list[i].address, ';');
	}
	return true;
}

bool write_file(string file_name, User* list)
{
	ofstream file(file_name);
	if (!file) {
		return false;
	}
	for (int i = 0; i < N; i++) {
		file << list[i].id << ";"
			<< list[i].name << ';'
			<< list[i].number << ';'
			<< list[i].plan << ';'
			<< list[i].address << ';' << endl;
	}
	return true;
}

void printf(User* list)
{
	for (int i = 0; i < N; i++) {
		cout << list[i].id << " "
			<< list[i].name << ' '
			<< list[i].number << ' '
			<< list[i].plan << ' '
			<< list[i].address << ' '<<endl;
	}
}

void about()
{
	cout << "������� ������ ���������" << endl 
		<< "�����-2-6" << endl 
		<< "16.12.2020"<< endl;
}
